#!/bin/bash

OUTDIR=$1

echo "Fetching SiteManager..."
./pullFromNexus.sh  -i com.cpn.cloud:sitemgr:1.0-SNAPSHOT -p jar -o $1

echo "Fetching Proxy..."
./pullFromNexus.sh  -i com.cpn.cloud.csm:wsProxy:1.0-SNAPSHOT -p jar -o $1

echo "Fetching WpsMgr..."
./pullFromNexus.sh  -i com.cpn.cloud.cue:wpsmgr:1.0-SNAPSHOT -p jar -o $1

echo "Fetching Dba..."
./pullFromNexus.sh  -i com.cpn.cloud.iss:DbaEngine:1.0-SNAPSHOT -p jar -o $1
scp -r senthila@192.168.128.182:~/Nightly-builds/LATEST/dba-engine/DbaEngine/resource-files ./app-files/dba/ 
rm -f ~/app-files/dba/rsrc.tgz
tar -cvzf ~/app-files/dba/rsrc.tgz -C ~/app-files/dba/resource-files .

echo "Fetching Ilc..."
./pullFromNexus.sh  -i com.cpn.cloud.iss:ilc:0.0.1-SNAPSHOT -p jar -o $1

echo "Fetching PoC..."
./pullFromNexus.sh  -i com.cpn.cloud.csm:pocsvr1:0.0.1-SNAPSHOT -p jar -o $1

echo "Fetching Eru..."
./pullFromNexus.sh  -i com.cpn.tam.eru:eru-parent:1.0-SNAPSHOT -p jar -o $1
./pullFromNexus.sh  -i com.cpn.tam.eru:facade:1.0-SNAPSHOT -p jar -o $1
./pullFromNexus.sh  -i com.cpn.tam.eru:flow-delta-processor:1.0-SNAPSHOT -p jar -o $1
./pullFromNexus.sh  -i com.cpn.tam.eru:runner:1.0-SNAPSHOT -p jar -o $1

echo "Fetching UI tarball..."
scp senthila@192.168.128.182:~/Nightly-builds/LATEST/cpn-ui/cpn-ui.tgz $1

echo "Fetching CDM setupOrchestration data..."
scp -r senthila@192.168.128.182:~/Nightly-builds/LATEST/CDM/cassandra/setupOrchestration ./app-files/cass/.
tar cvzf ./app-files/cass/setupOrch.tgz ./app-files/cass/setupOrchestration
