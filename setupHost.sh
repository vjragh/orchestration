#!/bin/sh

ovs-vsctl --may-exist add-br br-ext
ovs-vsctl --may-exist add-br br-int
ovs-vsctl --may-exist add-br br-mgmt
ifconfig  br-ext up
ifconfig  br-int up
ifconfig  br-mgmt up
ovs-vsctl --if-exist del-port br-ext enp0s3 
ovs-vsctl --may-exist add-port br-ext enp0s3
ovs-vsctl --if-exist del-port br-ext ovseth
ovs-vsctl --may-exist add-port br-ext ovseth -- set interface ovseth type=internal
ifconfig ovseth up
ifconfig enp0s3 0.0.0.0
dhclient ovseth
ifconfig
