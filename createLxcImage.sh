#!/bin/bash

usage()
{
cat <<EOF

usage: $0 options

OPTIONS:
   -a      ALL or [PXY | CASS| KAFKA | ERU | ILC | POC | DBA | SM | UI]
   -c      Install Cassandra Version x.y 
   -n      Install nginx
   -i      name of base image to build new container
   -o      name of the target container for the app
   -p      profile to apply to container

Ex:
 $0 -h
 $0 -b lxc-base -p test_profile
 $0 -a PXY -i lxc-base -o pxy-image -p test_profile

For non-interactive execution, do:
 yes | $0 -b lxc-base -p test_profile

EOF
}

CONTAINER_NAME=
IMAGE_NAME=
OUT_IMAGE_NAME=
CASSANDRA_VER=
NGINX=
APP_NAME=
BASE_IMG=
LXC_PROFILE=

while getopts "ha:b:c:i:o:p:" OPTION
do
     case $OPTION in
         h)
             usage
             exit 1
             ;;
         a)
             APP_NAME=$OPTARG
             CONTAINER_NAME=$OPTARG
             ;;
         b)
             CONTAINER_NAME=$OPTARG
             IMAGE_NAME=$OPTARG"-image"
             OUT_IMAGE_NAME=$OPTARG"-image"
             BASE_IMG=true
             ;;
         c)
             CASSANDRA_VER=$OPTARG
             ;;
         n)  
             NGINX=true
             ;;
         i)
             IMAGE_NAME=$OPTARG
             ;;
         o)
             OUT_IMAGE_NAME=$OPTARG
             ;;
         p)
             LXC_PROFILE=$OPTARG
             ;;
         ?)
             usage
             exit
             ;;
     esac
done

if [[ $BASE_IMG == "true" ]] && [[ -z $LXC_PROFILE ]] ; then
    echo "Creation of base image requires profile to be specified."
    usage 
    exit
fi

if [[ ! -z $APP_NAME ]] && ([[ -z $LXC_PROFILE ]] || [[ -z $IMAGE_NAME ]] || [[ -z OUT_IMAGE_NAME ]]) ; then
    echo "Creation of app image requires [profile, base-image, target-image] to be specified."
    usage 
    exit
fi

if [[ "$BASE_IMG" = "true" ]]
then
    echo "*** Creating Container ubuntu:16.04:amd64"
    lxc init images:ubuntu/xenial/amd64 $CONTAINER_NAME

    # applying the external profile so that it can connect to 
    # outside world, once the required packages are downloaded
    # we can switch back to int_app_profile
    #lxc profile assign $CONTAINER_NAME vin1_profile
    lxc profile assign $CONTAINER_NAME $LXC_PROFILE

    # start the container after applying profile
    echo "*** starting the container"
    lxc start $CONTAINER_NAME

    # get IP via dhcp on external interface
    lxc exec $CONTAINER_NAME -- ifconfig
    lxc exec $CONTAINER_NAME -- dhclient eth2
    lxc exec $CONTAINER_NAME -- ifconfig

    # update the container with java8 package
    echo "*** installing java8..."
    lxc file push ./OracleAutoAccept.sh $CONTAINER_NAME/root/OracleAutoAccept.sh
    lxc exec $CONTAINER_NAME -- chmod 744 /root/OracleAutoAccept.sh
    lxc exec $CONTAINER_NAME -- apt-get install software-properties-common
    lxc exec $CONTAINER_NAME -- add-apt-repository ppa:webupd8team/java
    lxc exec $CONTAINER_NAME -- apt-get update
    lxc exec $CONTAINER_NAME -- ./OracleAutoAccept.sh
    lxc exec $CONTAINER_NAME -- apt-get -q=2 -y install oracle-java8-installer

else 
    # launch a container based on the base image
    lxc launch $IMAGE_NAME $CONTAINER_NAME -p $LXC_PROFILE 

    # get IP via dhcp on external interface
    lxc exec $CONTAINER_NAME -- ifconfig
    lxc exec $CONTAINER_NAME -- dhclient eth2
    lxc exec $CONTAINER_NAME -- ifconfig

    # setup the container based on on the APP for which its being created
    # create a directory to put the start/stop scripts and other config files the app needs
    lxc exec $CONTAINER_NAME -- mkdir /root/$APP_NAME
    case $APP_NAME in
         PXY)
             lxc file push ./config-files/pxy-static-ip.cfg $CONTAINER_NAME/etc/network/interfaces.d/pxy-static-ip.cfg
             lxc file push ./config-files/pxy-interfaces $CONTAINER_NAME/etc/network/interfaces
             lxc file push ./config-files/wsProxy.service $CONTAINER_NAME/etc/systemd/system/wsProxy.service
             lxc file push ./images/wsProxy.jar $CONTAINER_NAME/root/$APP_NAME/wsProxy-1.0-SNAPSHOT.jar
             lxc file push ./app-files/pxy/Proxy.conf $CONTAINER_NAME/root/$APP_NAME/Proxy.conf
             lxc file push ./app-files/pxy/ProxyStart.sh $CONTAINER_NAME/root/$APP_NAME/ProxyStart.sh
             lxc file push ./app-files/pxy/start-Proxy.sh $CONTAINER_NAME/root/$APP_NAME/start-Proxy.sh
             lxc file push ./app-files/pxy/stop-Proxy.sh $CONTAINER_NAME/root/$APP_NAME/stop-Proxy.sh
             lxc file push ./app-files/pxy/pxyUpdateConf.sh $CONTAINER_NAME/root/$APP_NAME/pxyUpdateConf.sh
             lxc exec $CONTAINER_NAME -- /bin/sh -c "chmod 744 /root/PXY/pxyUpdateConf.sh"
             lxc file push ./app-files/pxy/cert.tgz  $CONTAINER_NAME/root/$APP_NAME/cert.tgz
             lxc exec $CONTAINER_NAME -- /bin/sh -c "tar xvzf /root/$APP_NAME/cert.tgz -C /root/PXY"
             lxc exec $CONTAINER_NAME -- /bin/sh -c "systemctl enable wsProxy.service"
             ;;
         SM)
             lxc file push ./config-files/sm-static-ip.cfg $CONTAINER_NAME/etc/network/interfaces.d/sm-static-ip.cfg
             lxc file push ./config-files/sm-interfaces $CONTAINER_NAME/etc/network/interfaces
             lxc file push ./config-files/sm.service $CONTAINER_NAME/etc/systemd/system/sm.service
             lxc file push ./images/sitemgr.jar $CONTAINER_NAME/root/$APP_NAME/sitemgr-1.0-SNAPSHOT.jar
             lxc file push ./app-files/sm/SiteManager-0.conf $CONTAINER_NAME/root/$APP_NAME/SiteManager-0.conf
             lxc file push ./app-files/sm/SMStart.sh $CONTAINER_NAME/root/$APP_NAME/SMStart.sh
             lxc file push ./app-files/sm/start-SM.sh $CONTAINER_NAME/root/$APP_NAME/start-SM.sh
             lxc file push ./app-files/sm/stop-SM.sh $CONTAINER_NAME/root/$APP_NAME/stop-SM.sh
             lxc file push ./app-files/sm/cert.tgz  $CONTAINER_NAME/root/$APP_NAME/cert.tgz
             lxc exec $CONTAINER_NAME -- /bin/sh -c "tar xvzf /root/$APP_NAME/cert.tgz -C /root/SM"
             lxc exec $CONTAINER_NAME -- /bin/sh -c "systemctl enable sm.service"
             ;;
        # poc edit not done
        # POC)
        #     lxc file push ./config-files/poc-static-ip.cfg $CONTAINER_NAME/etc/network/interfaces.d/poc-static-ip.cfg
        #     lxc file push ./config-files/poc-interfaces $CONTAINER_NAME/etc/network/interfaces
        #     lxc file push ./config-files/poc.service $CONTAINER_NAME/etc/systemd/system/poc.service
        #     lxc file push ./images/pocsvr1.jar $CONTAINER_NAME/root/$APP_NAME/pocsvr-0.0.1-SNAPSHOT.jar
        #     lxc file push ./app-files/poc/.conf $CONTAINER_NAME/root/$APP_NAME/SiteManager-0.conf
        #     lxc file push ./app-files/poc/SMStart.sh $CONTAINER_NAME/root/$APP_NAME/SMStart.sh
        #     lxc file push ./app-files/poc/start-SM.sh $CONTAINER_NAME/root/$APP_NAME/start-SM.sh
        #     lxc file push ./app-files/poc/stop-SM.sh $CONTAINER_NAME/root/$APP_NAME/stop-SM.sh
        #     lxc file push ./app-files/poc/cert.tgz  $CONTAINER_NAME/root/$APP_NAME/cert.tgz
        #     lxc exec $CONTAINER_NAME -- /bin/sh -c "tar xvzf /root/$APP_NAME/cert.tgz -C /root/SM"
        #     lxc exec $CONTAINER_NAME -- /bin/sh -c "systemctl enable sm.service"
        #     ;;
         WPS)
             lxc file push ./config-files/wps-static-ip.cfg $CONTAINER_NAME/etc/network/interfaces.d/wps-static-ip.cfg
             lxc file push ./config-files/wps-interfaces $CONTAINER_NAME/etc/network/interfaces
             lxc file push ./config-files/wps.service $CONTAINER_NAME/etc/systemd/system/wps.service
             lxc file push ./images/wpsmgr.jar $CONTAINER_NAME/root/$APP_NAME/wpsmgr-0.0.1-SNAPSHOT.jar
             lxc file push ./app-files/wps/WpsMgr.conf $CONTAINER_NAME/root/$APP_NAME/WpsMgr.conf
             lxc file push ./app-files/wps/WpsMgrStart.sh $CONTAINER_NAME/root/$APP_NAME/WpsMgrStart.sh
             lxc file push ./app-files/wps/start-WpsMgr.sh $CONTAINER_NAME/root/$APP_NAME/start-WpsMgr.sh
             lxc file push ./app-files/wps/stop-WpsMgr.sh $CONTAINER_NAME/root/$APP_NAME/stop-WpsMgr.sh
             lxc exec $CONTAINER_NAME -- /bin/sh -c "systemctl enable wps.service"
             ;;
         ERU)
             lxc file push ./config-files/eru-static-ip.cfg $CONTAINER_NAME/etc/network/interfaces.d/eru-static-ip.cfg
             lxc file push ./config-files/eru-interfaces $CONTAINER_NAME/etc/network/interfaces
             lxc file push ./config-files/eru.service $CONTAINER_NAME/etc/systemd/system/eru.service
             lxc file push ./images/runner.jar $CONTAINER_NAME/root/$APP_NAME/runner-1.0-SNAPSHOT.jat
             lxc file push ./app-files/eru/EventRollUp.conf $CONTAINER_NAME/root/$APP_NAME/EventRollUp.conf
             lxc file push ./app-files/eru/ERUStart.sh $CONTAINER_NAME/root/$APP_NAME/ERUStart.sh
             lxc file push ./app-files/eru/ERUStop.sh $CONTAINER_NAME/root/$APP_NAME/ERUStop.sh
             lxc exec $CONTAINER_NAME -- /bin/sh -c "systemctl enable eru.service"
             ;;
         ILC)
             lxc file push ./config-files/ilc-static-ip.cfg $CONTAINER_NAME/etc/network/interfaces.d/ilc-static-ip.cfg
             lxc file push ./config-files/ilc-interfaces $CONTAINER_NAME/etc/network/interfaces
             lxc file push ./config-files/ilc.service $CONTAINER_NAME/etc/systemd/system/ilc.service
             lxc file push ./images/ilc.jar $CONTAINER_NAME/root/$APP_NAME/ilc-1.0-SNAPSHOT.jar
             lxc file push ./app-files/ilc/Ilc.conf $CONTAINER_NAME/root/$APP_NAME/Ilc.conf
             lxc file push ./app-files/ilc/IlcStart.sh $CONTAINER_NAME/root/$APP_NAME/IlcStart.sh
             lxc file push ./app-files/ilc/start-Ilc.sh $CONTAINER_NAME/root/$APP_NAME/start-ILC.sh
             lxc file push ./app-files/ilc/stop-Ilc.sh $CONTAINER_NAME/root/$APP_NAME/stop-ILC.sh
             lxc exec $CONTAINER_NAME -- /bin/sh -c "systemctl enable ilc.service"
             ;;
         DBA)
             lxc file push ./config-files/dba-static-ip.cfg $CONTAINER_NAME/etc/network/interfaces.d/dba-static-ip.cfg
             lxc file push ./config-files/dba-interfaces $CONTAINER_NAME/etc/network/interfaces
             lxc file push ./config-files/dba.service $CONTAINER_NAME/etc/systemd/system/dba.service
             lxc file push ./images/DbaEngine.jar $CONTAINER_NAME/root/$APP_NAME/DbaEngine-0.0.1-SNAPSHOT.jar
             lxc file push ./app-files/dba/DbaEngine.conf $CONTAINER_NAME/root/$APP_NAME/DbaEngine.conf
             lxc file push ./app-files/dba/DbaEngineStart.sh $CONTAINER_NAME/root/$APP_NAME/DbaEngineStart.sh
             lxc file push ./app-files/dba/start-DbaEngine.sh $CONTAINER_NAME/root/$APP_NAME/start-DbaEngine.sh
             lxc file push ./app-files/dba/stop-DbaEngine.sh $CONTAINER_NAME/root/$APP_NAME/stop-DbaEngine.sh
             lxc file push ./app-files/dba/rsrc.tgz $CONTAINER_NAME/root/$APP_NAME/rsrc.tgz
             lxc exec $CONTAINER_NAME -- /bin/sh -c "mkdir /resource-files"
             lxc exec $CONTAINER_NAME -- /bin/sh -c "tar xvzf /root/DBA/rsrc.tgz -C /resource-files"
             lxc exec $CONTAINER_NAME -- /bin/sh -c "mkdir /classification-profiles"
             lxc exec $CONTAINER_NAME -- /bin/sh -c "systemctl enable dba.service"
             ;;
         UI)
             echo "*** installing nginx proxy"
             lxc exec $CONTAINER_NAME -- /bin/sh -c "mkdir /root/NGINX && rm -rf /root/UI"
             lxc file push ./config-files/ui-static-ip.cfg $CONTAINER_NAME/etc/network/interfaces.d/ui-static-ip.cfg
             lxc file push ./config-files/ui-interfaces $CONTAINER_NAME/etc/network/interfaces
             lxc file push ./config-files/user-acct.sh $CONTAINER_NAME/root/user-acct.sh  
             lxc file push ./app-files/ui/web-portal-dev.cpn.com $CONTAINER_NAME/root/NGINX/web-portal-dev.cpn.com
             lxc file push ./app-files/ui/nginxUpdateIp.sh $CONTAINER_NAME/root/NGINX/nginxUpdateIp.sh
             lxc file push ./app-files/ui/websocket.cpn.com $CONTAINER_NAME/root/NGINX/websocket.cpn.com
             lxc file push ./images/cpn-ui.tgz $CONTAINER_NAME/root/cpn-ui.tgz
             lxc file push ./nginx.sources.list $CONTAINER_NAME/etc/apt/sources.list.d/nginx.sources.list
             lxc exec $CONTAINER_NAME -- apt-get update
             lxc exec $CONTAINER_NAME -- apt-get install nginx
             ;;
         KAFKA)
             lxc file push ./config-files/kafka-static-ip.cfg $CONTAINER_NAME/etc/network/interfaces.d/kafka-static-ip.cfg
             lxc file push ./config-files/kafka-interfaces $CONTAINER_NAME/etc/network/interfaces
             lxc file push ./config-files/zookeeper.service $CONTAINER_NAME/etc/systemd/system/zookeeper.service
             lxc file push ./config-files/kafka.service $CONTAINER_NAME/etc/systemd/system/kafka.service
             lxc file push ./images/kafka_2.11-0.9.0.1.tgz $CONTAINER_NAME/root/$APP_NAME/kafka_2.11-0.9.0.1.tgz
             lxc file push ./app-files/kafka/KafkaStart.sh $CONTAINER_NAME/root/$APP_NAME/KafkaStart.sh
             lxc file push ./app-files/kafka/KafkaStop.sh $CONTAINER_NAME/root/$APP_NAME/KafkaStop.sh
             lxc file push ./app-files/kafka/ZooKeeperStart.sh $CONTAINER_NAME/root/$APP_NAME/ZooKeeperStart.sh
             lxc file push ./app-files/kafka/ZooKeeperStop.sh $CONTAINER_NAME/root/$APP_NAME/ZooKeeperStop.sh
             lxc file push ./app-files/kafka/startKafkaTopic.sh $CONTAINER_NAME/root/$APP_NAME/startKafkaTopic.sh  
             lxc exec $CONTAINER_NAME -- /bin/sh -c "tar xvzf /root/KAFKA/kafka_2.11-0.9.0.1.tgz -C /root/KAFKA"
             lxc file push ./app-files/kafka/kafkaUpdateServerProperties.sh $CONTAINER_NAME/root/KAFKA/kafkaUpdateServerProperties.sh
             lxc file push ./app-files/kafka/kafkaUpdateHosts.sh $CONTAINER_NAME/root/KAFKA/kafkaUpdateHosts.sh
             lxc exec $CONTAINER_NAME -- /bin/sh -c "systemctl enable zookeeper.service"
             lxc exec $CONTAINER_NAME -- /bin/sh -c "systemctl enable kafka.service"
             ;;
         CASS)
             echo "*** installing cassandra 3.0.x"
             lxc file push ./config-files/cass-static-ip.cfg $CONTAINER_NAME/etc/network/interfaces.d/cass-static-ip.cfg
             lxc file push ./config-files/cass-interfaces $CONTAINER_NAME/etc/network/interfaces
             lxc file push ./app-files/cass/setupOrch.tgz $CONTAINER_NAME/root/CASS/setupOrch.tgz
             lxc file push ./app-files/cass/cassandra-need-to-fix.yaml $CONTAINER_NAME/root/CASS/cassandra-need-to-fix.yaml
             lxc file push ./app-files/cass/cassandraUpdateYaml.sh $CONTAINER_NAME/root/CASS/cassandraUpdateYaml.sh
             lxc exec $CONTAINER_NAME -- /bin/sh -c "mkdir /root/CASS/app-files"
             lxc exec $CONTAINER_NAME -- /bin/sh -c "mkdir /root/CASS/app-files/cass"
             lxc exec $CONTAINER_NAME -- /bin/sh -c "mkdir /root/CASS/app-files/cass/setupOrchestration"
             lxc file push ./app-files/cass/loadCass.sh $CONTAINER_NAME/root/CASS/app-files/cass/setupOrchestration/loadCass.sh
             lxc exec $CONTAINER_NAME -- apt-get install curl
             lxc exec $CONTAINER_NAME -- sudo touch /etc/apt/sources.list.d/cassandra.sources.list
             #lxc exec $CONTAINER_NAME -- echo "deb http://www.apache.org/dist/cassandra/debian 30x main" | tee -a /etc/apt/sources.list.d/cassandra.sources.list
             lxc file push ./cassandra.sources.list $CONTAINER_NAME/etc/apt/sources.list.d/cassandra.sources.list
             lxc exec $CONTAINER_NAME -- curl https://www.apache.org/dist/cassandra/KEYS | apt-key add -
             #lxc exec $CONTAINER_NAME -- gpg --keyserver pgp.mit.edu --recv-keys F758CE318D77295D
             #lxc exec $CONTAINER_NAME -- gpg --export --armor F758CE318D77295D | sudo apt-key add -
             #lxc exec $CONTAINER_NAME -- gpg --keyserver pgp.mit.edu --recv-keys 2B5C1B00
             #lxc exec $CONTAINER_NAME -- gpg --export --armor 2B5C1B00 | sudo apt-key add -
             #lxc exec $CONTAINER_NAME -- gpg --keyserver pgp.mit.edu --recv-keys 0353B12C
             #lxc exec $CONTAINER_NAME -- gpg --export --armor 0353B12C | sudo apt-key add -
    
             lxc exec $CONTAINER_NAME -- apt-get update
             lxc exec $CONTAINER_NAME -- apt-get install cassandra
             echo "*** Done. do systemctl [start | stop | restart] cassandra"
             ;;
         ?)
             usage
             exit
             ;;
    esac

fi

## create container image
echo "*** stopping "$CONTAINER_NAME
# lxc stop <container> hangs after stopping the container, so trying to do
# a --force to stop. This is logged as back 8 days ago as of 12/7/2016 
lxc stop $CONTAINER_NAME --force
lxc list

echo "*** creating "$CONTAINER_NAME" base image"
lxc publish $CONTAINER_NAME --alias $OUT_IMAGE_NAME

echo "*** image created. check using: lxc image list"
echo "*** done."
