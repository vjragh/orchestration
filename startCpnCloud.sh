#!/bin/bash

usage()
{
cat <<EOF

usage: $0 options

OPTIONS:
   -h      Show this message
   -a      Application Options: [PXY | SM | WPS | ERU | ILC | DBA | KAFKA | CASS | UI | ALL]
   -s      <start | stop>
   
Ex:
 $0 -h
 $0 -a ALL  -s start     --> this will start all applictions in order

EOF
}

APP_NAME=
ACTION=

while getopts "ha:s:" OPTION
do
     echo $OPTION
     echo "***"
     case $OPTION in
         h)
             usage
             exit 1
             ;;
         a)
             APP_NAME=$OPTARG
             echo $APP_NAME
             echo "***"
             ;;
         s)
             ACTION=$OPTARG
             echo $ACTION
             echo "***"
             ;;
         ?)
             usage
             exit 1
             ;;
     esac
done

echo "###"
if [[ -z $APP_NAME ]]
then
    usage
    exit 1
fi

case $APP_NAME in
    ALL)
        if [[ $ACTION == "start" ]]; then
            echo "Starting Cassandra..."
            ./startContainers.sh -a CASS  -s start
            sleep 60
            echo "Starting Kafka..."
            ./startContainers.sh -a KAFKA -s start
            sleep 60
            echo "Starting ERU..."
            ./startContainers.sh -a ERU   -s start
            echo "Starting ILC..."
            ./startContainers.sh -a ILC   -s start
            echo "Starting DBA..."
            ./startContainers.sh -a DBA   -s start
            echo "Starting WPS..."
            ./startContainers.sh -a WPS   -s start
            echo "Starting Nginx..."
            ./startContainers.sh -a NGINX -s start
            echo "Starting SM..."
            ./startContainers.sh -a SM    -s start
            echo "Starting PXY..."
            ./startContainers.sh -a PXY   -s start
        else 
            ./startContainers.sh -a NGINX -s stop
            ./startContainers.sh -a PXY   -s stop
            ./startContainers.sh -a SM    -s stop
            ./startContainers.sh -a WPS   -s stop
            ./startContainers.sh -a DBA   -s stop
            ./startContainers.sh -a ERU   -s stop
            ./startContainers.sh -a ILC   -s stop
            ./startContainers.sh -a KAFKA -s stop
            ./startContainers.sh -a CASS  -s stop
        fi
        ;;
    ?)
        usage 
        exit 1
        ;;
esac

echo "***done."
