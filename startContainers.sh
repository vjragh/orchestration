#!/bin/bash

usage()
{
cat <<EOF

usage: $0 options

OPTIONS:
   -h      Show this message
   -a      Application Options: [PXY | SM | WPS | ERU | ILC | DBA | KAFKA | CASS | UI | ALL]
   -s      <start | stop>
   
Ex:
 $0 -h
 $0 -a PXY  -s start     --> this will only start PXY container
 $0 -a ALL  -s start     --> this will start all applictions in order

EOF
}

APP_NAME=
ACTION=

while getopts "ha:s:" OPTION
do
     echo $OPTION
     echo "***"
     case $OPTION in
         h)
             usage
             exit 1
             ;;
         a)
             APP_NAME=$OPTARG
             echo $APP_NAME
             echo "***"
             ;;
         s)
             ACTION=$OPTARG
             echo $ACTION
             echo "***"
             ;;
         ?)
             usage
             exit 1
             ;;
     esac
done

echo "###"
if [[ -z $APP_NAME ]]
then
    usage
    exit 1
fi

case $APP_NAME in
    PXY)
        if [[  $ACTION == "start" ]]
        then
            lxc config set local:PXY volatile.eth2.hwaddr 00:16:3e:00:00:01
            lxc start $APP_NAME
            #lxc exec $APP_NAME -- systemctl start wsProxy
        else
            lxc exec $APP_NAME -- systemctl stop wsProxy
            lxc stop $APP_NAME
        fi
        ;;
    SM)
        if [[  $ACTION == "start" ]]
        then
            lxc start $APP_NAME
            #lxc exec $APP_NAME -- systemctl start sm 
        else 
            lxc exec $APP_NAME -- systemctl stop sm
            lxc stop $APP_NAME
        fi
        ;;
    WPS)
        if [[  $ACTION == "start" ]]
        then
            lxc start $APP_NAME
            #lxc exec $APP_NAME -- systemctl start wps
        else
            lxc exec $APP_NAME -- systemctl stop wps 
            lxc stop $APP_NAME
        fi
        ;;
    ERU)
        if [[  $ACTION == "start" ]]
        then
            lxc start $APP_NAME
            #lxc exec $APP_NAME -- systemctl start eru
        else 
            lxc exec $APP_NAME -- systemctl stop eru
            lxc stop $APP_NAME
        fi
        ;;
    DBA)
        if [[  $ACTION == "start" ]]
        then
            lxc start $APP_NAME
            lxc exec DBA -- /bin/sh -c "tar xvzf /root/DBA/rsrc.tgz -C /resource-files"
            #lxc exec $APP_NAME -- systemctl start dba
        else
            lxc exec $APP_NAME -- systemctl stop dba
            lxc stop $APP_NAME
        fi
        ;;
    ILC)
        if [[  $ACTION == "start" ]]
        then
            lxc start $APP_NAME
            #lxc exec $APP_NAME -- systemctl start ilc
        else
            lxc exec $APP_NAME -- systemctl stop ilc
            lxc stop $APP_NAME
        fi
        ;;
    UI)
        if [[  $ACTION == "start" ]]
        then
            lxc config set local:NGINX volatile.eth2.hwaddr 00:16:3e:00:00:02
            lxc start NGINX

            # create user account
            lxc exec NGINX -- /bin/sh -c "chmod 744 /root/user-acct.sh && /root/user-acct.sh" 

            # get the ip address of the WPS MGR -- doesn't deal with multiple instance
            IPA=""
            while [ -z $IPA ]; do
                IPA=$(lxc exec WPS -- /bin/sh -c "ifconfig eth2 | grep 'inet addr:'" | cut -d: -f2 | awk '{print $1}')
            done
            lxc exec NGINX -- /bin/sh -c "sed -i 's/%%%%%%/'$IPA'/g' /root/NGINX/nginxUpdateIp.sh"
            lxc exec NGINX -- /bin/sh -c "mkdir /home/cpnadmin/cpn-ui"
            lxc exec NGINX -- /bin/sh -c "cd /root; tar xvzf /root/cpn-ui.tgz -C /home/cpnadmin/cpn-ui"
            lxc exec NGINX -- /bin/sh -c "chmod 744 /root/NGINX/nginxUpdateIp.sh && /root/NGINX/nginxUpdateIp.sh" 
        else
            lxc exec NGINX -- systemctl stop nginx
            lxc stop NGINX
        fi
        ;;
    KAFKA)
        if [[  $ACTION == "start" ]]
        then
            lxc start $APP_NAME
            IPA=""
            while [ -z $IPA ]; do
                IPA=$(lxc exec KAFKA -- /bin/sh -c "ifconfig eth2 | grep 'inet addr:'" | cut -d: -f2 | awk '{print $1}')
            done
            echo "kafka dhcp ip "$IPA
            lxc exec $APP_NAME -- /bin/sh -c "cd /root/KAFKA && /root/KAFKA/startKafkaTopic.sh" 
        else
            lxc exec $APP_NAME -- /bin/sh -c "systemctl stop kafka"
            lxc exec $APP_NAME -- /bin/sh -c "systemctl stop zookeeper"
            lxc stop $APP_NAME
        fi
        ;;
    CASS)
        if [[  $ACTION == "start" ]]
        then
            lxc start $APP_NAME
            # wait to get IP address
            while [ -z $IPA ]; do
                IPA=$(lxc exec CASS -- /bin/sh -c "ifconfig eth2 | grep 'inet addr:'" | cut -d: -f2 | awk '{print $1}')
            done
            echo "Cassandra dhcp ip "$IPA
            lxc exec $APP_NAME -- /bin/sh -c "chmod 744 /root/CASS/cassandraUpdateYaml.sh && /root/CASS/cassandraUpdateYaml.sh" 
            lxc exec $APP_NAME -- /bin/sh -c "cd /root/CASS; tar xvzf setupOrch.tgz"
            lxc exec $APP_NAME -- /bin/sh -c "systemctl stop cassandra"
            lxc exec $APP_NAME -- /bin/sh -c "systemctl start cassandra"
            echo "waiting 60s for cassandra to initialize..."
            sleep 60 
            lxc exec $APP_NAME -- /bin/sh -c "netstat -pnta"
            #lxc exec $APP_NAME -- /bin/sh -c "systemctl status cassandra"
            sleep 2
            lxc exec CASS -- /bin/sh -c "chmod 744 /root/CASS/app-files/cass/setupOrchestration/loadCass.sh && /root/CASS/app-files/cass/setupOrchestration/loadCass.sh"

        else
            lxc exec $APP_NAME -- systemctl stop cassandra 
            lxc stop $APP_NAME
        fi
        ;;
    ?)
        usage 
        exit 1
        ;;
esac

echo "***done."
