map $http_upgrade $connection_upgrade {
    default upgrade;
    '' close;
}

upstream websocket {
    # server localhost:8180;
    # to WPSMGR IP address
    server @@@@@@:8180;
}

server {
    listen 8092;
    location / {
        proxy_pass http://websocket;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection $connection_upgrade;
    }
}
