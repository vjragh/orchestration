#!/bin/bash
#update IP address of eth2
IPA=%%%%%%
sed 's/@@@@@@/'$IPA'/g' /root/NGINX/websocket.cpn.com > /etc/nginx/sites-available/websocket.cpn.com

cp /root/NGINX/web-portal-dev.cpn.com /etc/nginx/sites-available/web-portal-dev.cpn.com

# create softlinks
ln -s -t /etc/nginx/sites-enabled/ /etc/nginx/sites-available/web-portal-dev.cpn.com
ln -s -t /etc/nginx/sites-enabled/ /etc/nginx/sites-available/websocket.cpn.com

# restart the service
systemctl stop nginx
systemctl start nginx
