server {
    listen      8091 default_server;
    server_name web-portal-dev.cpn.com;
    root        /home/cpnadmin/cpn-ui/html;
    index       index.html index.htm; 
    location / {
        try_files $uri $uri/ =404;
    }
}
