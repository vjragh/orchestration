//==========================================================
// ILC optional tables
//==========================================================
DROP TABLE IF EXISTS config.sys_app_id;
CREATE TABLE config.sys_app_id (
    app_id      int,     //
    label       text,    // for UI
    PRIMARY KEY (app_id)
);

COPY config.sys_app_id (app_id,label) FROM 'sys_app_id.csv';

//==========================================================
DROP TABLE IF EXISTS config.ilc_group_defs;
CREATE TABLE config.ilc_group_defs (
    tenant_guid     text,
    type            text,       // group or template
    guid            text,       // group or template GUID
    label           text,       // label given by the Admin
    category        text,       // defined by tenant or system-default
    source          text,       // (true/false) can be Source of a flow (null means default to parent setting)
    dest            text,       // (true/false) can be Destination of a flow (null means default to parent setting)
    intra_policy    text,       // (true/false) can have intra-group/template policy (inter-group policy is always allowed) (null means default to parent setting)
    user_list       text,       // (true/false) can take username or userguid as memebrship
    subnet_list     text,       // (true/false) can take white-listed subnet, IP, or regex of either as membership
    device_list     text,       // (true/false) can take white-listed mac-addres as membership
    domain_list     text,       // (true/false) can take white-listed domain names as membership
    access_list     text,       // (true/false) can take access definitions as membership
    parent_guid     text,       // parent template's GUID (parent can not be a group, which is always a leaf-node in the template/group hierarchy)
    scope           text,       // policy scope of the group (tenant-wise, or site-only)
    image_url       text,       // image for the entity (UI only)
    PRIMARY KEY (tenant_guid,type,guid)
);

COPY config.ilc_group_defs (tenant_guid,type,guid,label,category,source,dest,intra_policy,user_list,
subnet_list,device_list,domain_list,access_list,parent_guid,scope,image_url
) FROM 'ilc_group_defs.csv';

//==========================================================
DROP TABLE IF EXISTS config.ilc_action_defs;
CREATE TABLE config.ilc_action_defs (
    tenant_guid     text,
    action_guid     text,       // action guid
    category        text,       // defined by tenant or system-default
    label           text,       // display string for UI
    code            text,       // code-string for inter-app usage
    parameters      text,       // parameter schema
    PRIMARY KEY (tenant_guid,action_guid)
);

COPY config.ilc_action_defs (tenant_guid,action_guid,category,label,code,parameters) FROM 'ilc_action_defs.csv';

//==========================================================
DROP TABLE IF EXISTS config.ilc_app_defs;
CREATE TABLE config.ilc_app_defs (
    tenant_guid     text,
    entry_guid      text,     //
    category        text,     // defined by tenant or system
    label           text,     // for UI
    app_id_list     text,     // JSON Array for list of snort app_id (from snort)
    PRIMARY KEY (tenant_guid,entry_guid)
);

COPY config.ilc_app_defs (tenant_guid,entry_guid,category,label,app_id_list) FROM 'ilc_app_defs.csv';


//==========================================================
DROP TABLE IF EXISTS config.ilc_policy_defs;
CREATE TABLE config.ilc_policy_defs (
    tenant_guid     text,
    policy_guid     text,     // policy guid
    rule_guid       text,     // rule guid
    priority        float,    // priority sequence (larger number means higher priority)
    type            text,     // inter or intra policy
    src_guid        text,     // source-group/template GUID (null means ANY)
    src_port        int,      // source-port
    dst_guid        text,     // dest-group/template GUID (only needed for inter-group, null means ANY)
    dst_port        int,      // dest-port
    iana_app_id     int,      // iana_app_id
    final_app_id    int,      // snort app_id
    app_def_guid    text,     // tenant-defined application groups (list of snort app_id). if both presented, union together.
    direction       text,     // simplex or duplex
    action_list     text,     // JSON string for list of action_guids
    PRIMARY KEY (tenant_guid,policy_guid, rule_guid, priority)
);

COPY config.ilc_policy_defs (tenant_guid,policy_guid, rule_guid,priority,type,src_guid,src_port,dst_guid,dst_port,
app_def_guid,iana_app_id,final_app_id,direction,action_list
) FROM 'ilc_policy_defs.csv';

//==========================================================
DROP TABLE IF EXISTS config.ilc_domain_defs;
CREATE TABLE config.ilc_domain_defs (
    tenant_guid         text,
    entry_guid          text,     //
    config_guid         text,     //
    label               text,     // for UI
    domain_name_list    text,     // JSON Array for list of domain name patterns
    PRIMARY KEY (tenant_guid,entry_guid)
);

COPY config.ilc_domain_defs (tenant_guid,entry_guid,config_guid,label,domain_name_list) FROM 'ilc_domain_defs.csv';

//==========================================================
DROP TABLE IF EXISTS config.ilc_subnet_defs;
CREATE TABLE config.ilc_subnet_defs (
    tenant_guid     text,
    entry_guid      text,     //
    type            text,     // ipv4, ipv6
    config_guid     text,     //
    label           text,     // for UI
    subnet_list     text,     // JSON Array for list of subnets
    regex_list      text,     // JSON Array for list of regular expression for matching subnets
    PRIMARY KEY (tenant_guid,entry_guid)
);

COPY config.ilc_subnet_defs (tenant_guid,entry_guid,type,config_guid,label,subnet_list,regex_list) FROM 'ilc_subnet_defs.csv';

//==========================================================
DROP TABLE IF EXISTS config.ilc_access_defs;
CREATE TABLE config.ilc_access_defs (
    tenant_guid     text,
    entry_guid      text,    //
    label           text,    // for UI
    config_guid     text,    //
    medium          text,    // lan, wan, wifi, usb, zigbee, zwave, ble,
    param           text,    // JSON Object for list of medium-specific attributes
    PRIMARY KEY (tenant_guid,entry_guid)
);

COPY config.ilc_access_defs (tenant_guid,entry_guid,label,config_guid,medium,param) FROM 'ilc_access_defs.csv';

//==========================================================
DROP TABLE IF EXISTS config.ilc_membership_defs;
CREATE TABLE config.ilc_membership_defs (
    tenant_guid         text,
    group_guid          text,
    access_defs_list    text,  // JSON array of ilc_access entry_guid
    subnet_defs_list    text,  // JSON array of ilc_subnet entry_guid
    domain_defs_list    text,  // JSON array of ilc_domain entry_guid
    mac_list            text,  // JSON array of MACs
    ip_list             text,  // JSON array of IPs
    PRIMARY KEY (tenant_guid,group_guid)
);

COPY config.ilc_membership_defs (tenant_guid,group_guid,access_defs_list,subnet_defs_list,domain_defs_list, mac_list, ip_list) FROM 'ilc_membership_defs.csv';

//==========================================================
DROP TABLE IF EXISTS config.ilc_site_scope_membership_defs;
CREATE TABLE config.ilc_site_scope_membership_defs (
    tenant_guid         text,
    site_guid           text,
    group_guid          text,
    access_defs_list    text,  // JSON array of ilc_access entry_guid
    subnet_defs_list    text,  // JSON array of ilc_subnet entry_guid
    domain_defs_list    text,  // JSON array of ilc_domain entry_guid
    PRIMARY KEY (tenant_guid,site_guid,group_guid)
);

COPY config.ilc_site_scope_membership_defs (tenant_guid,site_guid,group_guid,access_defs_list,subnet_defs_list,domain_defs_list) FROM 'ilc_site_scope_membership_defs.csv';

//==========================================================
DROP TABLE IF EXISTS config.ilc_group_device_whitelist;
CREATE TABLE config.ilc_group_device_whitelist (
    tenant_guid         text,
    group_guid          text,
    device_mac          text,
    flags               text,    // optional, JSON array of flags (strings)
    PRIMARY KEY (tenant_guid,group_guid,device_mac)
);

COPY config.ilc_group_device_whitelist (tenant_guid,group_guid,device_mac) FROM 'ilc_group_device_whitelist.csv';

//==========================================================
// This tables contains the Group->UserGuid mapping for only those users that are explicitly white-listed in our system.
// Not all the users are white-listed under group explicitly, and some of them may never be (e.g. guest). This table
// contains a subset of users that are explicitly configured into our system, and the entry persists even when the user
// is not connected to the system (cloud or network). 
// 
DROP TABLE IF EXISTS config.ilc_group_user_whitelist;
CREATE TABLE config.ilc_group_user_whitelist (
    tenant_guid         text,
    group_guid          text,
    user_guid           text,
    flags               text,   // optional, JSON array of flags (strings)
    PRIMARY KEY (tenant_guid,group_guid,user_guid)
);

COPY config.ilc_group_user_whitelist (tenant_guid,group_guid,user_guid) FROM 'ilc_group_user_whitelist.csv';

