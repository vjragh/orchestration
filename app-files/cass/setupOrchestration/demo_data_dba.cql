//==========================================================
// DBA (Device Behavior Analytics) Engine tables.
// These tables will also read (but not modified) by WpsMgr 
// in order to feed UI visualization. When Admin need to alter the opertaional parameters
// for the DBA, then the DBA application/service will be engaged. 
//==========================================================

//==========================================================
DROP TABLE IF EXISTS config.dba_auto_profile;
CREATE TABLE config.dba_auto_profile (
    tenant_guid         text,
    auto_profile_guid   text,
    auto_profile_name   text,  // profile name (given by ADMIN, or automatically named by the system)
    creation_ts         int,   // Milliseconds since EPOCH, when the entry is first created
    modified_ts         int,   // Milliseconds since EPOCH, when the entry is last modified
    learning_mode       text,  //
    inclusion_mode      text,  // ADMIN explicitly specified, system automatically detect under scope
    inclusion_scopes    text,  // JSON array of scope object, each indicates a particular scope (under SSX, AP, LAN, SSID, etc.) 
    division_list       text,  // JSON array of division guids
    descriptions        text,  // JSON array of text lines of descriptions
    ui_meta             text,  // JSON encoded object for UI meta data
    image_url           text,  // for UI display of device kind for this profile
    // other parameters to be added here
    PRIMARY KEY (tenant_guid,auto_profile_guid)
);

COPY config.dba_auto_profile (tenant_guid,auto_profile_guid,auto_profile_name,creation_ts,modified_ts,learning_mode,
inclusion_mode,inclusion_scopes,division_list,descriptions,ui_meta,image_url
) FROM 'dba_auto_profile.csv';

// NOTE: a single auto-profile starts with a list of devices that are to be learned.
// At the beginning, there will be only 1 division in the entire auto-profile with
// all the devices in there. Over time, we learn the device behavior, and will start to
// group communication end points (our devices and others) into "clusters. A division will
// then to start having multiple clusters. Some of the clusters are subset of the
// devices being learned, and we will create dbaProfile for that cluster, while end-point
// such as internet address that are not under our control, will not have DbaProfile created
// for that cluster. A cluster will be connecting to another clusters, and the entire communication
// graph will then be viewed at the cluster level. When the overall graph is paritioned into
// smaller disjoined sub-graph, each will be split into their own division, since each division
// are not communication with each other. Hence an AutoProfile will start from 1 division 1 cluster
// into 1 division with multiple clusters, and then multiple divisions each with smaller number
// of clusters.


//==========================================================
DROP TABLE IF EXISTS config.dba_auto_profile_division;
CREATE TABLE config.dba_auto_profile_division (
    tenant_guid         text,
    auto_profile_guid   text,
    division_guid       text,
    descriptions        text,  // JSON array of text lines of descriptions
    ref_devices         text,  // JSON array of device MAC-address (device-id) as the learning target
    cluster_list        text,  // JSON array of cluster guids
    PRIMARY KEY (tenant_guid,auto_profile_guid,division_guid)
);

COPY config.dba_auto_profile_division (tenant_guid,auto_profile_guid,division_guid,
descriptions,ref_devices,cluster_list) 
FROM 'dba_auto_profile_division.csv';

//==========================================================
DROP TABLE IF EXISTS config.dba_auto_profile_cluster;
CREATE TABLE config.dba_auto_profile_cluster (
    tenant_guid         text,
    auto_profile_guid   text,
    division_guid       text,
    cluster_guid        text,
    profile_guid        text,  // only when the ref_endpoints are devices under our control. 
    group_guid          text,  // a group can be created for this cluster that the policy might applied to it or to use profile for enforcement
    ref_endpoints       text,  // JSON array of endpoint_id
    PRIMARY KEY (tenant_guid,auto_profile_guid,division_guid,cluster_guid)
);

COPY config.dba_auto_profile_cluster (tenant_guid,auto_profile_guid,division_guid,cluster_guid,profile_guid,group_guid,ref_endpoints) 
FROM 'dba_auto_profile_cluster.csv';

//==========================================================
DROP TABLE IF EXISTS config.dba_dashboard;
CREATE TABLE config.dba_dashboard (
    tenant_guid         text,
    dashboard_data      text,
    PRIMARY KEY (tenant_guid)
);

//==========================================================
DROP TABLE IF EXISTS config.dba_endpoint;
CREATE TABLE config.dba_endpoint (
    // end point as a device for learning must be on-board first
    tenant_guid         text,
    endpoint_id         text,  // mac for devices under our control, IP/DomainName on others
    endpoint_type       text,  // the type of endpoint_id = MAC/IP/DNS 
    endpoint_name       text,  // optional, if endpoint_id is IP, this field is a reverse-DNS lookup (if no prior DNS mapping exist) of the IP at the source (SSX)
    auto_profile_guid   text,  // optional, if part of a auto_profile ref_devices
    division_guid       text,  // optional, if part of a auto_profile/division ref_devices
    cluster_guid        text,  // optional, if part of a auto_profile/division/cluster ref_devices
    profile_guid        text,  // optional, if part of a dba_profile ref_devices
    group_guid          text,  // optional, if part of a group
    orig_raw_vectors    set<text>,  // set of DBA-RawVector GUID originated from this end point, including DHCP, DNS, and Flow vectors
    term_raw_vectors    set<text>,  // set of of DBA-RawVector GUID terminated at this end point, including only Flow Vectors
    behvrprofile_guid   text,
    location_info       text,
    source_endpoint     boolean,
    group_name          text,
    learning_state      text,
    PRIMARY KEY (tenant_guid,endpoint_id)
);

COPY config.dba_endpoint (tenant_guid,endpoint_id,endpoint_type,endpoint_name,auto_profile_guid,
division_guid,cluster_guid,profile_guid,group_guid,learning_state,orig_raw_vectors,term_raw_vectors)
FROM 'dba_endpoint.csv';

//==========================================================
// Flow records reported from SSX that is related to the learning devices (origination most of the time, termination for some scenarios)
// will be consolidated into raw_vectors. Each raw_vector is for one pair of communicating end-points that at least one side is
// a device that is being learned.
// The raw-vector table is 
//
DROP TABLE IF EXISTS config.dba_raw_vector;
CREATE TABLE config.dba_raw_vector (
    tenant_guid         text,
    raw_vector_guid     text,
    orig_endpoint       text,  // origination endpoint_id of this raw_vector
    term_endpoint       text,  // termination endpoint_id of this raw_vector (optional 
    raw_vector_type     text,  // DHCP, DNS, Flow, RF, Site-Anchor, SSX-Anchor, SSID-Anchor, etc.
    vector_object       text,  // JSONified String of the vector contents
    //iana_protocol       int,   // always matter
    //service_app_id      int,   // optional
    //final_app_id        int,   // always matter
    //source_port         int,   // may not matter depending on protocol (such as TCP). If so, set to -1.
    //dest_port           int,   // matter if termEndPointGuid is non-null
    signature           text,  // JSON object of signature (orig-port, term-port, iana-protocol, service_app_id, final_app_id). Some of them is omitted depending on protocol
    meta                text,  // JSON object encoding of information captures from the flow-record, DNS-record, DHCP records
    vector_signature    int,  // A hash code computed based on relevant fields for the given vector.
    vector_stats                text,  // JSON object encoding of the cumulative statistics for this object
    incoming_conn       boolean,
    PRIMARY KEY (tenant_guid,raw_vector_guid)
);

CREATE INDEX orig_endopint_raw_vector ON config.dba_raw_vector (orig_endpoint);

COPY config.dba_raw_vector (tenant_guid,raw_vector_guid,orig_endpoint,term_endpoint,raw_vector_type,
vector_object,signature,meta) FROM 'dba_raw_vector.csv';

//==========================================================
DROP TABLE IF EXISTS config.dba_profile;
CREATE TABLE config.dba_profile (
    tenant_guid         text,
    profile_guid        text,
    profile_name        text,  // profile name (given by ADMIN)
    creation_ts         bigint,   // Milliseconds since EPOCH, when the entry is first created
    modified_ts         bigint,   // Milliseconds since EPOCH, when the entry is last modified
    mode                text,  // init/learning/inactive/active/blocked
    device_model        text,
    manufacturer        text,
    device_type         text,
    os_type             text,
    custom              boolean,
    tags                set<text>,
    fw_version          text,
    descriptions        text,  // JSON array of text lines of descriptions
    zone_list           text,  // JSON array of profile_zone guid(s)
    image_url           text,  // for UI display of device kind for this profile
    ref_devices         text,  // JSON array of devices (MAC) used for building up this profile
    ref_device_count    int,
    convergence         text,  // JSON object about convergence status and detail info
    strict_enforcement  boolean,
    push_config         boolean,
    // other parameters to be added here
    PRIMARY KEY (tenant_guid,profile_guid)
);

COPY config.dba_profile (tenant_guid,profile_guid,profile_name,creation_ts,modified_ts,mode,device_model,
manufacturer,fw_version,descriptions,zone_list,image_url,ref_devices,ref_device_count,convergence
) FROM 'dba_profile.csv';

//==========================================================
DROP TABLE IF EXISTS config.dba_profile_zone;
CREATE TABLE config.dba_profile_zone (
    tenant_guid         text,
    profile_guid        text,
    profile_zone_guid   text,
    label               text,
    cadence_element     text,  // GUID of cadence vector element 
    element_list        text,  // JSON array of vector element_guid 
    ui_meta             text,  // JSON encoded meta data for UI rendering 
    // other parameters to be added here
    PRIMARY KEY (tenant_guid,profile_guid,profile_zone_guid)
);

COPY config.dba_profile_zone (tenant_guid,profile_guid,profile_zone_guid,label,cadence_element,element_list,ui_meta ) FROM 'dba_profile_zone.csv';

//==========================================================
DROP TABLE IF EXISTS config.dba_vector_element;
CREATE TABLE config.dba_vector_element (
    tenant_guid         text,
    profile_guid        text,
    element_guid        text,
    type                text,  // DHCP, DNS, flow, cadence, schedule, etc.
    name                text,  // optional name of element (given by ADMIN)
    creation_ts         bigint,   // Milliseconds since EPOCH, when the entry is first created
    modified_ts         bigint,   // Milliseconds since EPOCH, when the entry is last modified 
    descriptions        text,  // JSON array of text lines of descriptions
    ui_meta             text,  // JSON encoded meta data for UI rendering 
    ancestor_elements   text,  // JSON array of element_guid(s) that are consider ancestor as result of inter-element dependency analysis
    data_set            text,  // JSON encoded data set (each data is named and contains a 2-D array of data). 
    // other parameters to be added here
    PRIMARY KEY (tenant_guid,profile_guid,element_guid)
);

COPY config.dba_vector_element (tenant_guid,profile_guid,element_guid,type,name,creation_ts,modified_ts,descriptions,
ui_meta,ancestor_elements,data_set
) FROM 'dba_vector_element.csv';


//==========================================================
// When profile_guid is set to "DBA-PROFILE-LIST", the blob contains the list of profile summary for the given tenant
// This table is generated by the DBA Engine, and consumed by the WpsMgr on behave of CPN-UI. DBA-Engine itself
// does not consume information stored in this table.
//
DROP TABLE IF EXISTS config.dba_profile_bundle;
CREATE TABLE config.dba_profile_bundle (
    tenant_guid         text,
    profile_guid        text,
    bundle_blob         text,
    PRIMARY KEY (tenant_guid,profile_guid)
);

COPY config.dba_profile_bundle (tenant_guid,profile_guid,bundle_blob) FROM 'dba_profile_bundle.csv';

DROP TABLE IF EXISTS config.dba_behavior_profile; 
CREATE TABLE config.dba_behavior_profile (
    vector_guid         text,
    match_vector        text,
    group_tags          set<text>,
    PRIMARY KEY (vector_guid)
);

DROP TABLE IF EXISTS config.dba_device_info; 
CREATE TABLE config.dba_device_info (
    client_mac          text,
    device_info         text,      
    PRIMARY KEY (client_mac)
);
