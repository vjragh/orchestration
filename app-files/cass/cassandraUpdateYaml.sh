#!/bin/bash

IPA=""
while [ -z $IPA ]; do
    IPA=$(ifconfig eth2 | grep 'inet addr:' | cut -d: -f2 | awk '{print $1}')
done
echo "Cassandra dhcp IP "$IPA
sed 's/@@@@@@/'$IPA'/g' /root/CASS/cassandra-need-to-fix.yaml > /etc/cassandra/cassandra.yaml
