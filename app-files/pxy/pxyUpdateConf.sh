#!/bin/bash
IPA=
while [ -z $IPA ]; do
    IPA=$(ifconfig eth2 | grep 'inet addr:' | cut -d: -f2 | awk '{print $1}')
done
sed -i 's/@@@@@@/'$IPA'/g' /root/PXY/Proxy.conf

