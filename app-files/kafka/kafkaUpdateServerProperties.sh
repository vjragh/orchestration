#!/bin/bash
IPA=
while [ -z $IPA ]; do
    IPA=$(ifconfig eth2 | grep 'inet addr:' | cut -d: -f2 | awk '{print $1}')
done
IPZ=0.0.0.0
sed -i 's/PLAINTEXT:\/\/:/PLAINTEXT:\/\/'$IPZ':/g' /root/KAFKA/kafka_2.11-0.9.0.1/config/server.properties

#comment the listeners, listens on all interfaces
#sed -ie '/listeners=PLAINTEXT:/s/^/#/g' /root/KAFKA/kafka_2.11-0.9.0.1/config/server.properties

# update and uncomment the line that contains host.name
#IP=10.1.1.201
#sed -i 's/host.name=localhost/host.name=10.1.1.201/' /root/KAFKA/kafka_2.11-0.9.0.1/config/server.properties
#sed -ie '/^#host.name=/s/^#\(.*\)/\1/' /root/KAFKA/kafka_2.11-0.9.0.1/config/server.properties

# update the advertised host name
IP=10.1.1.201
sed -ie '/^#advertised.host.name=/s/^#advertised.host.name=\(.*\)/advertised.host.name='$IP'/' /root/KAFKA/kafka_2.11-0.9.0.1/config/server.properties

