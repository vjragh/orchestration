#!/bin/sh

cd /root/KAFKA/kafka_2.11-0.9.0.1; bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partition 1 --topic flowStats

sleep 2;

cd /root/KAFKA/kafka_2.11-0.9.0.1; bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partition 1 --topic event

sleep 2;

cd /root/KAFKA/kafka_2.11-0.9.0.1; bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partition 1 --topic deviceEvents

sleep 2;

cd /root/KAFKA/kafka_2.11-0.9.0.1; bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partition 1 --topic ilc

sleep 2;

cd /root/KAFKA/kafka_2.11-0.9.0.1; bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partition 1 --topic dba

sleep 2;

cd /root/KAFKA/kafka_2.11-0.9.0.1; bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partition 1 --topic wpsmgr

sleep 2;

cd /root/KAFKA/kafka_2.11-0.9.0.1; bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partition 1 --topic config-update
