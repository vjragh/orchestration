#!/bin/sh
IPA=
sed -i '/KAFKA/d' /etc/hosts
while [ -z $IPA ]; do
    IPA=$(ifconfig eth2 | grep 'inet addr:' | cut -d: -f2 | awk '{print $1}')
done
echo $IPA
name=`hostname`
hosts_entry="${IPA} ${name}"
echo ${hosts_entry} >> /etc/hosts
